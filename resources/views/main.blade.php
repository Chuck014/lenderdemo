<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Lender Homepage Demo</title>
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
        <script src="{{ mix('/js/manifest.js') }}"></script>
        <script src="{{ mix('/js/vendor.js') }}"></script>
    </head>
    <body>
        <div id="demo">
            <b-container>
                <h1 class="text-center">
                    <small class="text-muted">Demo for:</small> Lender Homepage
                </h1>
                <h5 class="text-center">Prepared By: <a href="https://walkerdevelopment.ca" target="_blank">Michael Walker</a></h5>
                <b-card no-body>
                    <b-tabs pills card vertical>
                        <b-tab v-for="(team, index) in teams" :active="index == 0" :key="index">
                            <template slot="title">
                                @{{ team.name }}
                                <font-awesome-icon class="float-right" :icon="['far', 'trash-alt']" @click="deleteTeam(index)"></font-awesome-icon>
                                <font-awesome-icon class="float-right" :icon="['far', 'edit']" @click="showTeamModal(index)"></font-awesome-icon>
                            </template>

                            <b-form-group horizontal :label-cols="4" label="Team Name:">
                                <b-form-input :value="team.name" plaintext></b-form-input>
                            </b-form-group>

                            <b-form-group horizontal :label-cols="4" label="Established:">
                                <b-form-input :value="moment(team.created_at).format('MMMM Do YYYY, h:mm:ss a')" plaintext></b-form-input>
                            </b-form-group>

                            <b-form-group horizontal :label-cols="4" label="Last Roster Change:">
                                <b-form-input :value="moment(team.updated_at).format('MMMM Do YYYY, h:mm:ss a')" plaintext></b-form-input>
                            </b-form-group>

                            <h4 class="text-center">Roster:</h4>
                            <b-card-group columns>
                                <b-card v-for="(player, index2) in team.players" :key="index2">
                                    @{{ player.first_name }} @{{ player.last_name }}
                                    <font-awesome-icon class="float-right" :icon="['far', 'trash-alt']" @click="deletePlayer(index, index2)"></font-awesome-icon>
                                    <font-awesome-icon class="float-right" :icon="['far', 'edit']" @click="showPlayerModal(index, index2)"></font-awesome-icon>
                                </b-card>

                                <b-card @click="showPlayerModal(index, -1)" bg-variant="info">
                                    Add Player
                                </b-card>
                            </b-card-group>
                        </b-tab>

                        <b-tab>
                            <template slot="title">
                                Add New
                            </template>
                        </b-tab>
                    </b-tabs>
                </b-card>

            </b-container>

            <b-modal ref="playerModal" title="Player" @ok="addPlayer" centered>
                <b-form-group label="First Name">
                    <b-form-input type="text" v-model="modal.player.first_name"></b-form-input>
                </b-form-group>

                <b-form-group label="Last Name">
                    <b-form-input type="text" v-model="modal.player.last_name"></b-form-input>
                </b-form-group>

                <b-form-group label="Team">
                    <model-list-select :list="teams" v-model="modal.player.teamId" option-value="id" option-text="name"></model-list-select>
                </b-form-group>
            </b-modal>

            <b-modal ref="teamModal" title="Team" @ok="addTeam" centered>
                <b-form-group label="Name">
                    <b-form-input type="text" v-model="modal.team.name"></b-form-input>
                </b-form-group>
            </b-modal>
        </div>

        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
