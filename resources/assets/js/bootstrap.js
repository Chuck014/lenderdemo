import Vue from 'vue';
import Moment from 'moment';
import Axios from 'axios';
import BootstrapVue from 'bootstrap-vue';
import { ModelListSelect } from 'vue-search-select';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
library.add(
	faEdit,
	faTrashAlt
);

window.Vue = Vue;
Vue.use(BootstrapVue);
Vue.component(FontAwesomeIcon.name, FontAwesomeIcon);
Vue.component('model-list-select', ModelListSelect);

window.axios = Axios;
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
};
window.axios.defaults.baseURL = '/api/';

Vue.prototype.$http = Axios;
Vue.prototype.moment = moment;
