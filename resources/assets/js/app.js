import './bootstrap.js';

var vue = new Vue({
    el: '#demo',
    data: {
        teams: [],
        modal: {
            player: {
                id: 0,
                first_name: '',
                last_name: '',
                teamId: 0
            },
            team: {
                id: 0,
                name: ''
            }
        },
        passphrase: '',
    },
    methods: {
        getPassphrase() {
            if (this.passphrase == '') {
                this.passphrase = prompt('Enter the API Key for write access.');
            }
        },
        resetPassPhrase() {
            this.passphrase = '';
            alert('Invalid API Key, please try again');
        },
    	loadData() {
	        this.$http.get('/teams/players').then(({ data: data }) => {
	            this.teams = data.teams;
	        });
    	},
        showTeamModal($teamIndex) {
            if ($teamIndex == -1) {
                this.modal.team.id = 0;
                this.modal.team.name = '';
            } else {
                this.modal.team.id = this.teams[$teamIndex].id;
                this.modal.team.name = this.teams[$teamIndex].name;
            }

            this.$refs.teamModal.show();
        },
        addTeam() {
            this.getPassphrase();
            let data = {
                name: this.modal.team.name,
                api_token: this.passphrase
            };
            if (this.modal.team.id == 0) {
                this.$http.post('teams', data).then(results => {
                    this.loadData();
                }, this).catch(error => {
                    this.resetPassPhrase();
                }, this);
            } else {
                this.$http.put('teams/' + this.modal.team.id, data).then(results => {
                    this.loadData();
                }, this).catch(error => {
                    this.resetPassPhrase();
                }, this);
            }

            this.$refs.teamModal.hide();
        },
        deleteTeam($teamIndex) {
            this.getPassphrase();
            this.$http.delete('teams/' + this.teams[$teamIndex].id + '?api_token=' + this.passphrase).then(results => {
                this.loadData();
            }, this).catch(error => {
                this.resetPassPhrase();
            }, this);
        },
        showPlayerModal($teamIndex, $playerIndex) {
            this.modal.player.teamId = this.teams[$teamIndex].id;

            if ($playerIndex == -1) {
                this.modal.player.id = 0;
                this.modal.player.first_name = '';
                this.modal.player.last_name = '';
            } else {
                this.modal.player.id = this.teams[$teamIndex].players[$playerIndex].id;
                this.modal.player.first_name = this.teams[$teamIndex].players[$playerIndex].first_name;
                this.modal.player.last_name = this.teams[$teamIndex].players[$playerIndex].last_name;
            }

            this.$refs.playerModal.show();
        },
        addPlayer() {
            this.getPassphrase();
            let data = {
                first_name: this.modal.player.first_name,
                last_name: this.modal.player.last_name,
                api_token: this.passphrase
            };
            if (this.modal.player.id == 0) {
                this.$http.post('teams/' + this.modal.player.teamId + '/players', data).then(results => {
                    this.loadData();
                }, this).catch(error => {
                    this.resetPassPhrase();
                }, this);
            } else {
                this.$http.put('teams/' + this.modal.player.teamId + '/players/' + this.modal.player.id, data).then(results => {
                    this.loadData();
                }, this).catch(error => {
                    this.resetPassPhrase();
                }, this);
            }

            this.$refs.playerModal.hide();
        },
        deletePlayer($teamIndex, $playerIndex) {
            this.getPassphrase();
            this.$http.delete('teams/' + this.teams[$teamIndex].id + '/players/' + this.teams[$teamIndex].players[$playerIndex].id + '?api_token=' + this.passphrase).then(results => {
                this.loadData();
            }, this).catch(error => {
                this.resetPassPhrase();
            }, this);
        }
    },
    mounted() {
    	this.loadData();
    }
});
