<?php

namespace App\Http\Controllers;

use App\Team;
use App\Player;
use App\Http\Requests\Player as PlayerRequest;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PlayerController extends Controller
{
    /**
     * Create a new team after going through validation in the PlayerRequest validator
     *
     * @param integer $teamId Team Id
     * @param PlayerRequest $newPlayer Player information
     * @return json
     */
    public function create($teamId, PlayerRequest $newPlayer)
    {
        $player = new Player;
        $player->first_name = $newPlayer->first_name;
        $player->last_name = $newPlayer->last_name;
        $player->team()->associate($teamId);
        $player->save();

        return response()->json(['players' => $player], 201);
    }

    /**
     * Delete a player from a specific team
     *
     * @param integer $teamId Team Id
     * @param integer $id Player Id
     * @return void
     */
    public function delete($teamId, $id)
    {
        $team = Team::with(['players' => function ($query) use ($id) {
            $query->where('id', $id);
        }])->find($teamId);

        $team->players->first()->delete();

        response()->json([], 204);
    }

    /**
     * Update an existing player information and team, once validated in PlayerRequest
     *
     * @param integer $teamId Team Id
     * @param integer $id Player Id to update
     * @param PlayerRequest $updatePlayer Updated player information
     * @return json
     */
    public function update($teamId, $id, PlayerRequest $updatePlayer)
    {
        try {
            $player = Player::with('team')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->create($teamId, $updatePlayer);
        }

        $player->first_name = $updatePlayer->first_name;
        $player->last_name = $updatePlayer->last_name;

        if ($player->team->id != $teamId) {
            $player->team()->associate($teamId);
        }

        $player->save();

        return response()->json(['players' => $player], 200);
    }

    /**
     * Get a list of all players on a team
     *
     * @param integer $teamId Team Id
     * @return json
     */
    public function list($teamId)
    {
        $team = Team::with('players')->findOrFail($teamId);

        return response()->json(['players' => $team->players], 200);
    }

    /**
     * Get a specific player on a team
     *
     * @param integer $teamId Team Id
     * @param integer $id Player Id
     * @return json
     */
    public function get($teamId, $id)
    {
        $team = Team::with(['players' => function ($query) use ($id) {
            $query->where('id', $id);
        }])->find($teamId);

        return response()->json(['players' => $team->players->first()], 200);
    }
}
