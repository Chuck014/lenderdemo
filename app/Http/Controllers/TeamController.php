<?php

namespace App\Http\Controllers;

use App\Team;
use App\Http\Requests\Team as TeamRequest;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TeamController extends Controller
{
    /**
     * Create a new team after going through validation in the TeamRequest validator
     *
     * @param TeamRequest $new Request object after validation
     * @return json
     */
    public function create(TeamRequest $new)
    {
        $team = Team::create([
            'name' => $new->name,
        ]);

        return response()->json(['teams' => $team], 201);
    }

    /**
     * Gets a list of all teams
     *
     * @return json
     */
    public function list()
    {
        return response()->json(['teams' => Team::all()], 200);
    }

    /**
     * Get a list of all teams and their players
     *
     * @return json
     */
    public function listPlayers()
    {
        return response()->json(['teams' => Team::with('players')->get()], 200);
    }

    /**
     * Get a single team
     *
     * @param integer $id Team Id
     * @return json
     */
    public function get($id)
    {
        return response()->json(['teams' => Team::findOrFail($id)], 200);
    }

    /**
     * Update a team, or create if id not found
     *
     * @param integer $id Team Id
     * @param TeamRequest $updatedTeam Updated information on the team
     * @return json
     */
    public function update($id, TeamRequest $updatedTeam)
    {
        try {
            $team = Team::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->create($updatedTeam);
        }

        $team->name = $updatedTeam->name;
        $team->save();

        return response()->json(['teams' => $team], 200);
    }

    /**
     * Delete a team
     *
     * @param integer $id Team Id
     * @return void
     */
    public function delete($id)
    {
        $team = Team::findOrFail($id);
        $team->players()->delete();
        $team->delete();

        response()->json([], 204);
    }
}
