<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * Get the players on the team.
     */
    public function players()
    {
        return $this->hasMany('App\Player');
    }
}
