<?php

// Route::post('login', 'PassportController@login');
// Route::post('register', 'PassportController@register');

Route::get('teams/players', 'TeamController@listPlayers');

Route::get('teams', 'TeamController@list');
Route::get('teams/{id}', 'TeamController@get');

Route::get('teams/{teamId}/players', 'PlayerController@list');
Route::get('teams/{teamId}/players/{id}', 'PlayerController@get');

Route::middleware('apiToken:api')->group(function () {
    // Route::get('user', 'PassportController@details');

    Route::post('teams', 'TeamController@create');
    Route::put('teams/{id}', 'TeamController@update');
    Route::delete('teams/{id}', 'TeamController@delete');

    Route::post('teams/{teamId}/players', 'PlayerController@create');
    Route::put('teams/{teamId}/players/{id}', 'PlayerController@update');
    Route::delete('teams/{teamId}/players/{id}', 'PlayerController@delete');
});
