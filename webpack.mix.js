let mix = require('laravel-mix');

mix.autoload({
    'moment': [
        'moment',
        'window.moment',
        'global.moment'
    ]
});

mix.js('resources/assets/js/app.js', 'public/js');

mix.extract([
    'axios',
    'bootstrap',
    'bootstrap-vue',
    'moment',
    'vue'
]);

mix.sass('resources/assets/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}
